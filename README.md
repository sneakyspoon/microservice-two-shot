# Wardrobify

Team:

* Jenn Trotter - Hats
* Adam Fortin - Shoes

## Design

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

## Hats microservice

The Hat model stores the required data for each hat (fabric,
style, location, etc.) and integrates with the wardrobify
project via the LocationVO model, which stores a virtual object
of each location so we can track hat storage without affecting
the Locations in wardrobe.

Viewing/deleting hats is done in the HatsList.js file, which
is in the nav as "Hats".  This file includes:
- a HatsList component, which creates the columns and includes
    a loop to populate a card for each hat via:
- a HatColumn component, which creates and populates a card.
    The card includes a button to delete the hat, via:
- the DeleteHat function

Creating a hat is done in the HatsForm.js file, which is in
the nav as "Create a hat."  This file includes the function
HatsForm, which uses State to manage user inputs and store
them on a newly created Hat object.
