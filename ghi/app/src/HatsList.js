import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'

async function DeleteHat(id) {
    const url = `http://localhost:8090/api/hats/${id}/`
    const fetchConfig = {
        method: "DELETE",
    }
    const response = await fetch(url, fetchConfig)
    if (response.ok) {
        window.location.reload(false)
    } else {
        console.error(response);
    }
}

function HatColumn(props) {
    return(
      <div className='col'>
        {props.list.map(data => {
          const hat = data
          return (
            <div key={hat.id} className='card mb-3 shadow'>
              <img src={hat.picture_url} className='card-img-top' alt='' />
              <div className='card-body'>
                <h5 className='card-title'>{hat.style_name}</h5>
                <h6 className='card-subtitle mb-2 text-muted'>{hat.fabric} - {hat.color}</h6>
                <button type="button" className="btn btn-outline-danger mx-3" onClick={() => DeleteHat(hat.id)}>Delete</button>
              </div>
              <div className='card-footer'>
                {hat.location.closet_name}
              </div>
            </div>
          )
        })}
      </div>
    )
  }

function HatsList() {
  const [hatColumns, setHatColumns] = useState([[], [], []])

  const fetchData = async () => {
    const url = 'http://localhost:8090/api/hats/'

    try {
      const response = await fetch(url)
      if (response.ok) {
        const data = await response.json()
        const requests = []
        for (let hat of data.hats) {
          const detailUrl = `http://localhost:8090/api/hats/${hat.id}`
          requests.push(fetch(detailUrl))
        }

        const responses = await Promise.all(requests)
        const columns = [[], [], []]
        let i = 0
        for (const hatResponse of responses) {
          if (hatResponse.ok) {
            const details = await hatResponse.json()
            columns[i].push(details)
            i = i + 1
            if (i > 2) {
              i = 0
            }
          } else {
            console.error(hatResponse)
          }
        }
        setHatColumns(columns)
      }
    } catch (e) {
      console.error(e)
    }
  }

  useEffect(() => {
    fetchData()
  }, [])

  return(
    <div className='container'>
        <h2>Hats</h2>
        <div className='row'>
            {hatColumns.map((hatList, index) => {
                return (
                    <HatColumn key={index} list={hatList} />
                )
            })}
        </div>
    </div>
  )
}

export default HatsList
