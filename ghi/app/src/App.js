import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesForm from './ShoesForm';
import ShoesList from './ShoesList';
import HatsForm from './HatsForm';
import HatsList from './HatsList';

function App() {
  const [shoes, setShoes] = useState([]);


async function getShoes() {
  const response = await fetch('http://localhost:8080/api/shoes/');
  if (response.ok) {
    const { shoes } = await response.json();
    setShoes(shoes);
  } else {
    console.error('An Error occurred fetching the data');
  }
}
useEffect(() => {
  getShoes();
}, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoesList />} />
          <Route path="/create-shoes" element={<ShoesForm />} />
          <Route path="/hats" element={<HatsList />} />
          <Route path="/create-hats" element={<HatsForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}


export default App;
