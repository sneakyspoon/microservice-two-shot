from django.db import models


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)
    bin_number = models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    model = models.CharField(max_length=200)
    color = models.CharField(max_length=50)
    picture_url = models.URLField()
    location = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )


    def __str__(self):
        return self.shoe_name
