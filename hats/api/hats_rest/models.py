from django.db import models


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, default="")
    closet_name = models.CharField(max_length=200, default="")


class Hat(models.Model):
    style_name = models.CharField(max_length=200)
    fabric = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name='location',
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.style_name
